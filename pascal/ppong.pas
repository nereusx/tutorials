{ ping-pong, all-time classic }
Program PingPong4B(input, output);
Uses Graph, crt;

{	allagi se graphics mode
	Sim: sto directory tou programmatos tha prepei na yparxei kai to arxeio egavga.bgi }
Procedure InitGraphDev;
Var
	bgiDriver, bgiMode, bgiError : Integer;
Begin
bgiDriver := Detect;
InitGraph(bgiDriver, bgiMode, '');
bgiError := GraphResult;
if bgiError <> grOK then
	Begin
	WriteLn('Graphics driver error: ', GraphErrorMsg(bgiError));
	Halt(1);	{ amesi eksodos }
	End;
End;

{ epistrofi se text mode }
Procedure CloseGraphDev;
Begin
CloseGraph;
End;

{ mia sfaira pou anapida stis akres tis othonis }
Procedure PingPong;
Var	ch : Char;
	xMax, yMax, x, y, r, xDir, yDir : Integer;
	hit	: Boolean;
Begin

{ adiasma tou keyboard buffer (diabazoume osa pliktra yparxoun) }
While KeyPressed Do ch := ReadKey;

{ arxikes times }
xMax := GetMaxX;
yMax := GetMaxY;
x := xMax div 2;	{ orizontia thesi tis sfairas }
y := yMax div 2;	{ katheti thesi tis sfairas }
xDir := 1;			{ orizontia dieuthinsi tis sfairas (1 `h -1) }
yDir := -1;			{ katheti dieuthinsi tis sfairas (1 `h -1) }
r := 3;				{ aktina tis sfairas }

Rectangle(0, 0, xMax, yMax);	{ zwgrafizoume to plaisio }

While not KeyPressed Do	{ efoson den exei patithei pliktro }
	Begin
	hit := false;

	{ an exei ftasei sta oria, allazoume tin fora tis }
	if (x + r + 1 >= xMax) OR (x - (r + 1) <= 0) then
		begin xDir := -xDir; hit := true; end;
	if (y + r + 1 >= yMax) OR (y - (r + 1) <= 0) then
		begin yDir := -yDir; hit := true; end;

	{ sbinoume tin proigoumeni sfaira (zwgrafizoume me mauro xrwma) }
	SetColor(Black);
	Circle(x, y, r);

	{ ypologizoume tin nea thesi }
	x := x + xDir;	{ basika to swsto einai x := x + epithimito_bima * xDir,
						alla epeidi exoume bima 1 den exei noima }
	y := y + yDir;

	{ zwgrafizoume tin sfaira }
	SetColor(White);
	Circle(x, y, r);

	{ epeidi einai poly grigoro, bazoume mia kathisterisi twn 100ms (10 pix/sec) }
	if hit then Sound(440);
	Delay(100);
	if hit then NoSound;
	End;
End;

{ main }
Begin
InitGraphDev;	{ gyrna se graphics mode }
PingPong;		{ paikse mpala }
CloseGraphDev;	{ gyrna se text mode }
End.
