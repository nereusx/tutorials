{ mantepse ton arithmo ... }
Program SecretNum(input, output);

Var secret  : Integer;	{ o kryfos arithmos }
	usernum : Integer;	{ o arithmos pou pliktrologei o xristis }
	jerkcnt : Integer;	{ oi prospathies tou xristi }

Begin
	Randomize;
	secret  := Random(100);
	jerkcnt := 0;
	WriteLn('Mantepse ton kryfo arithmo (0..99)');

	Repeat
		ReadLn(usernum);
		jerkcnt := jerkcnt + 1;
		if usernum > secret then
			WriteLn('einai mikroteros')
		else
			if usernum < secret then
				WriteLn('einai megalyteros');
	Until secret = usernum;

	WriteLn('Bravo! to brikes me ', jerkcnt, ' prospathies!')
End.
