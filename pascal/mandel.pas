{	To perifimo fractal tou Polonou mathimatikou
	http://www-groups.dcs.st-and.ac.uk/~history/Mathematicians/Mandelbrot.html }

Program MandelBrot(input, output);
Uses Graph, crt;

{	allagi se graphics mode
	Sim: sto directory tou programmatos tha prepei na yparxei kai to arxeio egavga.bgi }
Procedure InitGraphDev;
Var
	bgiDriver, bgiMode, bgiError : Integer;
Begin
bgiDriver := Detect;
InitGraph(bgiDriver, bgiMode, '');
bgiError := GraphResult;
if bgiError <> grOK then
	Begin
	WriteLn('Graphics driver error: ', GraphErrorMsg(bgiError));
	Halt(1);	{ amesi eksodos }
	End;
End;

{ epistrofi se text mode }
Procedure CloseGraphDev;
Begin
CloseGraph;
End;

{   Perimenei na patithei ena pliktro
	An patithei ESC tote diakoptei to programma me Halt (amesi eksodos) }
Procedure Pause;
Var	ch : Char;
Begin
{ diabase ola ta pliktra apo tin keyboard buffer }
While KeyPressed Do ch := ReadKey;
{ perimene na patithei ena pliktro }
ch := ReadKey;
if ch = #27 then { an patithike to [ESC] kleise to programma }
	Halt(1);
End;

{ sxediasmos tou fractal }
Procedure DrawMandel;
Var	MaxColors, clr : Integer;
	xMax, yMax, yHalf : Integer;
	LeftSide, TopSide, xSide, ySide : Real;
	xScale, yScale : Real;
	cx, zx, nzx : Real;
	cy, zy : Real;
	x, y : Integer;

Begin
{ graphics device info }
MaxColors := GetMaxColor + 1;
xMax  := GetMaxX;
yMax  := GetMaxY;
yHalf := yMax div 2;

{ border and scale }
LeftSide  := -2;
TopSide   := 1.24;
xSide  := 2.5;
ySide  := -2.5;
xScale := xSide / xMax;
yScale := ySide / yMax;

{ draw }
For y := 1 To yHalf Do
	Begin
	For x := 1 To xMax Do
		Begin
		cx  := x * xScale + LeftSide;
		cy  := y * yScale + TopSide;
		zx  := 0; zy := 0;
		clr := 0;
		While (zx*zx + zy*zy < 4) AND (clr < MaxColors) Do
			Begin
			nzx := zx*zx - zy*zy + cx;
			zy  := 2*zx*zy + cy;
			zx  := nzx;
			clr := clr + 1;
			End;

		PutPixel(x, y, clr);
		PutPixel(x, yMax-y, clr);
		End;
	End;
End;

{ kentriko programma }
Begin
InitGraphDev;	{ gyrna se graphics mode }
DrawMandel;		{ zwgrafise to fractal }
Pause;			{ perimene apo ton xristi na patisei ena pliktro }
CloseGraphDev;	{ gyrna se text mode }
End.

