Program TelBook4Bebe;
Uses crt, dos;

Type TilEggrafi =
	Record
	Onoma : String;
	Dieuth : String;
	Til : LongInt;
	End;

Var
	TilBook : Array [1..100] of TilEggrafi;	{ to biblio }
	Synolo  : Integer;						{ arithmos kataxwrimenwn eggrafwn }
	epilogi : Integer;

{   Perimenei na patithei ena pliktro
	An patithei ESC tote diakoptei to programma me Halt (amesi eksodos) }
Procedure Pause;
Var	ch : Char;
Begin
{ diabase ola ta pliktra apo tin keyboard buffer }
While KeyPressed Do ch := ReadKey;
{ perimene na patithei ena pliktro }
ch := ReadKey;
if ch = #27 then { an patithike to [ESC] kleise to programma }
	Halt(1);
End;

Function YparxeiToArxeio(OnomaArxeiou : String) : Boolean;
Var		DirInfo: SearchRec;
Begin
FindFirst(OnomaArxeiou, Archive, DirInfo);
YparxeiToArxeio := (DosError = 0);
End;

Procedure NeaEggrafi;
Begin
ClrScr;
WriteLn('Eisagwgi eggrafis');
WriteLn('-----------------');

Synolo := Synolo + 1;
Write('Onoma:      ');	ReadLn(TilBook[Synolo].Onoma);
Write('Dieuthynsi: ');	ReadLn(TilBook[Synolo].Dieuth);
Write('Tilefwno:   ');	ReadLn(TilBook[Synolo].Til);

WriteLn('H eggrafi kataxwrithike stin thesi: ', Synolo);
Delay(2000);
End;

Procedure TypwseKatalogo;
Var		i : Integer;
Begin
ClrScr;
WriteLn('Ektypwsi listas');
WriteLn('---------------');
For i := 1 To Synolo do
	WriteLn(i, ': ', TilBook[i].Onoma, ', ', TilBook[i].Dieuth, ', ', TilBook[i].Til);

WriteLn('Telos... Patise ena pliktro gia epistrofi');
Pause;
End;

Procedure ApothikeusiStonDisko;
Var		arxeio : Text;
		i : Integer;
Begin
Assign(arxeio, 'tbook.dat');
Rewrite(arxeio);
WriteLn(arxeio, Synolo);
For i := 1 To Synolo do
	Begin
	WriteLn(arxeio, TilBook[i].Onoma);
	WriteLn(arxeio, TilBook[i].Dieuth);
	WriteLn(arxeio, TilBook[i].Til);
	End;
Close(arxeio);
End;

Procedure DiabasmaApoDisko;
Var	arxeio : Text;
	i : Integer;
Begin
if YparxeiToArxeio('tbook.dat') then
	begin
	Assign(arxeio, 'tbook.dat');
	Reset(arxeio);
	ReadLn(arxeio, Synolo);
	For i := 1 To Synolo do
		Begin
		ReadLn(arxeio, TilBook[i].Onoma);
		ReadLn(arxeio, TilBook[i].Dieuth);
		ReadLn(arxeio, TilBook[i].Til);
		End;
	Close(arxeio);
	End;
End;

Procedure DiorthwsiEggrafis;
Var	eggr : Integer;
Begin
ClrScr;
WriteLn('Diorthwsi eggrafis');
WriteLn('------------------');
Write('Epilekste ton arithmo tis eggrafis: ');
ReadLn(eggr);
if (eggr <= 0) or (eggr > synolo) then
	Begin
	WriteLn('Den yparxei kataxwrisi');
	Delay(2000);
	End
else
	Begin
	WriteLn(eggr, ': ', TilBook[eggr].Onoma, ', ', TilBook[eggr].Dieuth, ', ', TilBook[eggr].Til);
	WriteLn;
	Write('Onoma:      ');	ReadLn(TilBook[eggr].Onoma);
	Write('Dieuthynsi: ');	ReadLn(TilBook[eggr].Dieuth);
	Write('Tilefwno:   ');	ReadLn(TilBook[eggr].Til);
	WriteLn('H eggrafi kataxwrithike stin thesi: ', eggr);
	Delay(2000);
	End;
End;

Begin
Synolo := 0;
DiabasmaApoDisko;
Repeat
	ClrScr;
	WriteLn('TilBook Menu:');
	WriteLn('-----------------------');
	WriteLn('1 .. Eisagwgi tilefwnou');
	WriteLn('2 .. Diorthwsi eggrafis');
	WriteLn('3 .. Typwma katalogou');
	WriteLn('0 .. Eksodos');
	ReadLn(epilogi);
	if epilogi = 1 then	NeaEggrafi;
	if epilogi = 2 then DiorthwsiEggrafis;
	if epilogi = 3 then TypwseKatalogo;
Until epilogi = 0;
ApothikeusiStonDisko;
End.
