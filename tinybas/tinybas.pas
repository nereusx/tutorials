{mia akoma tinybasic alla se Pascal auti tin fora
btw, to manual tis original tinybasic: http://www.ittybittycomputers.com/IttyBitty/TinyBasic/TBuserMan.htm
leitourgei gia TurboPascal kai FreePascal... se alles ekdoseis den to exw dokimasei...
Nicholas Christopoulos, Feb 2005}
Program TinyBASIC(input, output);

Const MaxStackNode = 31;	{ megistos arithmos kombou tis stack }
Const MaxProgLine = 100;	{ megistos arithmos grammis BASIC pou mporei na fylaxtei stin mnimi }
Const TBVersion = '1.0';	{ h ekdosh... }

Type ProgLineT =
	Record
	LineNo : Integer;	{ arithmos grammis BASIC }
	Code   : String;	{ entoli }
	End;

Var
	VarVal			: Array [65..90] of Real;		{ oi 26 metablites (mia gia kathe gramma) }
	ProgLines		: Array [1..MaxProgLine] of ProgLineT;	{ grammes kwdika tou xristi }
	ProgLinesCount	: Integer;						{ arithmos eggrafwn sto ProgLines }
	CurProgLine		: Integer;						{ h trexousa grammin otan ektelite to programma tou xristi `h < 1 an exei stamatisei }
	ExecStack		: Array [0..MaxStackNode] of Integer; 	{ stack (gia ta gosub/return)}
	ExecStackCount	: Integer;						{ 'stack pointer' }
	RTErrorStatus	: Boolean;						{ an true proklithike sfalma kata tin ektelesi tis teleutaias entolis }	
	TraceStatus		: Boolean;						{ tron/troff status }

{ --- PART I --- GENIKES SYNARTISEIS --------------------------- }
	
{ run-time error }
Procedure RTError(msg : String);
Begin
RTErrorStatus := true;
if CurProgLine > 0 then
	WriteLn('* ', msg, ', (GRAMMH ', ProgLines[CurProgLine].LineNo, ') *')
else
	WriteLn('* ', msg, ' *');
End;

{ Metatroph enos string se kefalaia }
Function StrUpCase(src : String) : String;
Var	len, i : Integer;
Begin
len := Length(src);
For i := 1 To len Do
	src[i] := UpCase(src[i]);
StrUpCase := src;
End;

{ Epistrefei to prwto keyword sto expr }
Function GetNextKeyword(expr : String; Var i : Integer) : String;
Var		len : Integer;
		kword  : String;
Begin
len := Length(expr);
i := 1;
kword := '';
While (i <= len) AND (expr[i] in [#32,#9]) Do i := i + 1;	{ arxika kena }
While (i <= len) AND (Pos(UpCase(expr[i]), 'ABCDEFGHIJKLMNOPQRSTUVWXYZ_0123456789') > 0) Do
	Begin
	kword := kword + expr[i];
	i := i + 1;
	End;
While (i <= len) AND (expr[i] in [#32,#9]) Do i := i + 1;	{ kena }
{expr := Copy(expr, i, len);}
GetNextKeyword := kword;
End;

{ epistrefei to src alla xwris kena stin aristeri pleura }
Function LTrim(src : String) : String;
Var	len, i : Integer;
Begin
len := length(src);
i := 1;
While (i <= len) AND (src[i] in [#32,#9]) Do i := i + 1;
LTrim := Copy(src, i, len)
End;

{ epistrefei true an to string 'src' periexei akaireo arithmo }
Function IsInt(src : String) : Boolean;
Var	i, len : Integer;
Begin
len := Length(src);
IsInt := true;
For i := 1 To len Do
	if Pos(src[i], '0123456789') = 0 then
		IsInt := false;
End;

{ epistrefei true an to string 'src' periexei latinika grammata kai mono }
Function IsAlpha(src : String) : Boolean;
Var	i, len : Integer;
Begin
len := Length(src);
IsAlpha := true;
For i := 1 To len Do
	if NOT (
		((src[i] >= #65) AND (src[i] <= #90))
		OR
		((src[i] >= #97) AND (src[i] <= #122))
		) then
		IsAlpha := false;
End;

{ apothikeui stin stack }
Procedure ExecPush(data : Integer);
Begin
if ExecStackCount = MaxStackNode then
	RTError('YPERXYLISH SWROU')
else
	Begin
	ExecStack[ExecStackCount] := data;
	ExecStackCount := ExecStackCount + 1;
	End;
End;

{ epistrefei to top node kai to afairei apo tin lista }
Function ExecPop : Integer;
Begin
if ExecStackCount = 0 then
	RTError('O SWROS EINAI ADEIOS')
else
	Begin
	ExecStackCount := ExecStackCount - 1;
	ExecPop := ExecStack[ExecStackCount];
	End;
End;

{ --- PART II --- Ektelesi math. ekfrasewn ----------------------- }

Procedure EvalLogical(VAR result : Real; VAR expr : String); Forward;

{ arithmitiki timi }
Function EvalValue(VAR expr : String) : Real;
Var	parm : String;
	i, len, c : Integer;
	r	: Real;

Begin
len := Length(expr);
i := 1;
parm := '';
While (i <= len) AND (Pos(expr[i], '0123456789.') > 0) Do
	Begin
	parm := parm + expr[i];
	i := i + 1;
	End;

expr := LTrim(Copy(expr, i, Length(expr)));
Val(parm, r, c);
EvalValue := r;
End;

{ parentheseis, functions `h metablites }
Procedure EvalParenth(VAR result : Real; VAR expr : String);
Var 	op, name : String;
		idx      : Integer;
		right    : Real;
Begin
op := expr[1];
if op = '(' then
	Begin
	expr := LTrim(Copy(expr, 2, Length(expr)));
	EvalLogical(result, expr);
	if expr[1] = ')' then
		expr := LTrim(Copy(expr, 2, Length(expr)));
	End
else
	Begin
	if Pos(op, '0123456789.') > 0 then
		result := EvalValue(expr)
	else if (Ord(op[1]) >= 65) AND (Ord(op[1]) <= 90) then
		{ metabliti `h synartisi }
		Begin
		idx    := 1;
		name   := GetNextKeyword(expr, idx);
		expr   := LTrim(Copy(expr, idx, Length(expr)));
		right  := 0;		
		if name = 'PI' then
			result := PI
		else if name = 'RND' then
			Begin
			EvalParenth(right, expr);
			result := (Random(30000)/30000) * right;
			End
		else if name = 'COS' then
			Begin
			EvalParenth(right, expr);
			result := cos(right);
			End			
		else if name = 'SIN' then
			Begin
			EvalParenth(right, expr);
			result := sin(right);
			End	
		else if name = 'SQR' then
			Begin
			EvalParenth(right, expr);
			result := sqrt(right);
			End			
			
		else if Length(name) > 1 then
			RTError('AGNWSTH SYNARTHSH')
		else
			result := VarVal[Ord(name[1])];
		End;
	End;
End;

{ monadikoi }
Procedure EvalUnary(VAR result : Real; VAR expr : String);
Var	op : String;
Begin
if Copy(expr, 1, 3) = 'NOT' then
	Begin
	op := '!';
	expr := LTrim(Copy(expr, 4, length(expr)));
	End
else if expr[1] in ['-', '+'] then
	Begin
	op := expr[1];
	expr := LTrim(Copy(expr, 2, length(expr)));
	End;

EvalParenth(result, expr);

if op = '!' then
	Begin
	if result = 0 then
		result := 1
	else
		result := 0;
	End
else if op = '-' then
	result := -result;
End;

{ pollaplasiasmos/diairesi }
Procedure EvalMul(VAR result : Real; VAR expr : String);
Var	op : String;
	right : Real;
Begin
EvalUnary(result, expr);
While (Length(expr) > 0) AND (expr[1] in ['*', '/', '%']) Do
	Begin
	op := expr[1];
	expr := LTrim(Copy(expr, 2, Length(expr)));
	right := 0;
	EvalUnary(right, expr);
	if op = '*' then
		result := result * right
	else
		Begin
		if right = 0 then
			Begin
			WriteLn('* DIAIRESH ME TO MHDEN *');
			result := 0;
			End
		else
			Begin
			if op = '/' then
				result := result / right
			else
				result := Trunc(result) MOD Trunc(right);
			End;
		End;
	End;
End;

{ prosthesi/afairesh }
Procedure EvalAdd(VAR result : Real; VAR expr : String);
Var	op : String;
	right : Real;
Begin
EvalMul(result, expr);
While (Length(expr) > 0) AND (expr[1] in ['+', '-']) Do
	Begin
	op := expr[1];
	expr := LTrim(Copy(expr, 2, Length(expr)));
	right := 0;
	EvalMul(right, expr);
	if op = '+' then
		result := result + right
	else
		result := result - right;
	End;
End;

{ an einai epitreptos telestis tote epistrefei ton telesti alliws keno string }
Function GetLogOp(expr : String) : String;
Var	op2, op3 : String;
Begin
op3 := Copy(expr, 1, 3); 
op2 := Copy(expr, 1, 2);
if op3 = 'AND' then GetLogOp := op3
else if op2 = 'OR' then GetLogOp := op2
else if op2 = '<>' then GetLogOp := op2
else if op2 = '<=' then GetLogOp := op2
else if op2 = '>=' then GetLogOp := op2
else if expr[1] = '=' then GetLogOp := expr[1]
else if expr[1] = '>' then GetLogOp := expr[1]
else if expr[1] = '<' then GetLogOp := expr[1]
else GetLogOp := '';
End;

{ logikoi telestes & telestes sygkrisis ... bariemai na tous diaxwrisw - anoikoun se diaforetiko epipedo }
Procedure EvalLogical(VAR result : Real; VAR expr : String);
Var	op    : String;
	right : Real;
Begin
EvalAdd(result, expr);
op := GetLogOp(expr);
While (Length(expr) > 0) AND (op <> '') Do
	Begin
	expr  := LTrim(Copy(expr, Length(op)+1, Length(expr)));
	right := 0;
	EvalAdd(right, expr);
	if op = 'AND' then
		Begin
		if (result <> 0) AND (right <> 0) then result := 1 else result := 0;
		End
	else if op = 'OR' then
		Begin
		if (result <> 0) OR (right <> 0) then result := 1 else result := 0;
		End
	else if op = '>=' then
		Begin
		if result >= right then result := 1 else result := 0;
		End
	else if op = '<=' then
		Begin
		if result <= right then result := 1 else result := 0;
		End
	else if op = '<' then
		Begin
		if result < right then result := 1 else result := 0;
		End
	else if op = '>' then
		Begin
		if result > right then result := 1 else result := 0;
		End
	else if op = '=' then
		Begin
		if result = right then result := 1 else result := 0;
		End
	else if op = '<>' then
		Begin
		if result <> right then result := 1 else result := 0;
		End

	End;
End;

{ evaluate an expression (main) }
Function Eval(expr : String) : Real;
Var	result : Real;
Begin
result := 0;
expr := StrUpCase(LTrim(expr));
if Length(expr) > 0 then
	EvalLogical(result, expr);
Eval := result;
End;

{ --- PART III --- Entoles tis BASIC ----------------------- }

Function Execute(cmd : String; params : String) : Boolean; Forward;

{ katharizei tin mnimi kai to programma (antistoixi tis 'new') }
Procedure ResetBASIC;
Var		i : Integer;
Begin
ProgLinesCount := 0;		{ kammia eggrafi sto ProgLines }
For i := 65 To 90 Do		{ midenismos twn metablitwn }
	VarVal[i] := 0;
CurProgLine := -1;
ExecStackCount := 0;
RTErrorStatus := false;
TraceStatus := false;
End;

{ h entoli new ... oti kai h ResetBASIC }
Procedure CmdNew(parms : String);
Begin
ResetBASIC;
WriteLn;
WriteLn('* READY FOR SEX *');
End;

{ diaxeiristis tis mnimis pou apothikeuetai to programma tou xristi }
Procedure SetProgLine(LineNo : Integer; Code : String);
Var		i : Integer;

	{ prosthiki sto telos }
	Procedure AddAtEnd;
	Begin
	if Code <> '' then
		Begin
		ProgLinesCount := ProgLinesCount + 1;
		ProgLines[ProgLinesCount].LineNo := LineNo;
		ProgLines[ProgLinesCount].Code := Code;	
		End;
	End;

	{ prosthiki mias grammis stin thesi idx }
	Procedure InsertLine(idx : Integer);
	Var	i : Integer;
	Begin
	For i := ProgLinesCount DownTo idx Do
		ProgLines[i+1] := ProgLines[i];

	ProgLines[idx].LineNo := LineNo;
	ProgLines[idx].Code := Code;	
	ProgLinesCount := ProgLinesCount + 1;	
	End;

	{ antikatastasi mias grammis }
	Procedure ReplaceLine(idx : Integer);
	Begin	
	ProgLines[idx].LineNo := LineNo;
	ProgLines[idx].Code := Code;	
	End;

	{ diagrafi mias grammis }
	Procedure RemoveLine(idx : Integer);
	Var	i : Integer;
	Begin
	For i := idx To ProgLinesCount Do
		ProgLines[i] := ProgLines[i+1];
	ProgLinesCount := ProgLinesCount - 1;
	End;
	
Begin
if ProgLinesCount = 0 then			{ den yparxei alli eggrafi }
	AddAtEnd
else if ProgLines[ProgLinesCount].LineNo < LineNo then { o arithmos einai megaliteros apo ton teleutaio }
	AddAtEnd
else
	Begin
	i := 1;
	While ProgLines[i].LineNo < LineNo Do
		i := i + 1;

	if Code = '' then
		Begin
		if ProgLines[i].LineNo = LineNo then
			RemoveLine(i);
		End
	else
		Begin
		if ProgLines[i].LineNo = LineNo then
			ReplaceLine(i)
		else
			InsertLine(i);
		End;
	End;
End;

{ to typiko help }
Procedure DispHelp;
Begin
WriteLn('TinyBASIC se Pascal (TBP), Nikolas Xristopoulos, Feb 2005, ver ', TBVersion);
WriteLn('--- Entoli ------------------------ Perigrafi --------------------------');
WriteLn('HELP                                Emfanizei tis entoles tis TBP');
WriteLn('QUIT                                Termatizei tin TBP');
WriteLn('CLEAR/NEW                           Katharizei tin mnimi tis TBP');
WriteLn('PR/PRINT [expr|"string" [,|; ...]]  emfanisi stin othoni');
WriteLn('INPUT [var1[, var2 ...]]            eisagwgi timwn apo to pliktrologio');
WriteLn('LET var = expr                      orismos timis se metabliti');
WriteLn('GOTO line                           metafora tis ektelesis');
WriteLn('GOSUB line                          metafara tis ektelesis me dynatotia epistrofis');
WriteLn('RETURN                              epistrofi apo gosub');
WriteLn('IF expr THEN command                ektelesi tou (command) an to (expr) einai mi mideniko');
WriteLn('REM                                 comments');
WriteLn('LIST                                lista tou programmatos tou xristi');
WriteLn('SAVE <file>                         apothikeusi');
WriteLn('LOAD <file>                         anagnwsi h/kai ektelesi');
WriteLn('RUN                                 ektelesi programmatos xristi');
WriteLn('END                                 stamatima tis ektelesis');
WriteLn('TRON/TROFF                          energopoiisi/apenergopoiisi tis emfaniseis twn grammwn kata tin ektelesi');
WriteLn('--- Synartiseis ---');
WriteLn('RND(pedio)                          tyxaios arithmos');
WriteLn('SIN(gwnia)                          imitono');
WriteLn('COS(gwnia)                          synimitono');
WriteLn('SQR(x)                              tetragwniki riza');
WriteLn('--- Simeiwseis ---');
WriteLn('Ta onomata twn metablitwn einai 1 gramma (opws stin original)');
WriteLn('Oi metablites einai typou kinitis ypodiastolis (Real) panta.');
End;

{ h entolh PRINT 
	syntax: print [expr|"string" [,|; ...]] }
Procedure CmdPrint(src : String);
Var	len, i : Integer;
	parm   : String;
	parmIsStr, quotes : Boolean;
	r      : Real;
Begin
len := length(src);
i := 1;

parm := '';
parmIsStr := false;
quotes := false;
While i <= len Do
	Begin
	if ((not quotes) AND (src[i] in [',', ';'])) OR (i = len) then	{ o charactiras einai diaxwristis }
		Begin
		if (i = len) AND (NOT (src[i] in [',', ';', '"'])) then
			parm := parm + src[i];

		if length(parm) > 0 then
			Begin
			{ typwse }
			if parmIsStr then
				Write(parm)
			else
				Begin
				r := Eval(parm);
				if r - Trunc(r) = 0 then
					Write(Trunc(r))
				else if (r * 1000) - (Trunc(r * 1000)) = 0 then
					Write(r:0:3)
				else
					Write(r);
				End;

			{ afise kena analoga ton diaxwristi }
			if src[i] = ',' then
				Write(#9);

			{ proetoimasia gia nea leksi }
			parmIsStr := false;
			parm := '';
			End;
		End
	else
		Begin
		if src[i] = '"' then
			Begin
			quotes := not quotes;
			parmIsStr := true;
			End
		else
			parm := parm + src[i];
		End;

	i := i + 1;
	End;

if src[Length(src)] <> ';' then
	WriteLn;
End;

{ h entolh INPUT 
	syntax: INPUT [var1[, var2 ...] }
Procedure CmdInput(parms : String);
Var	buf : String;
	pidx, bidx : Integer;
	vname, vvalue : String;
Begin
ReadLn(buf);
While (Length(parms) > 0) AND (RTErrorStatus = false) Do
	Begin
	pidx := 1;
	vname := StrUpCase(GetNextKeyword(parms, pidx));
	if Length(vname) = 0 then
		RTError('SYNTAKTIKO SFALMA STHN INPUT')
	else
		Begin
		if parms[1] = ',' then pidx := pidx + 1;
		parms := LTrim(Copy(parms, pidx, Length(parms)));
		
		{ pare to tmima tou buf }
		bidx := 1;
		vvalue := StrUpCase(GetNextKeyword(buf, bidx));
		if buf[1] = ',' then bidx := bidx + 1;		
		buf := LTrim(Copy(buf, bidx, Length(buf)));
		
		{ apothikeuse tin timi tou vvalue }
		VarVal[Ord(vname[1])] := Eval(vvalue);
		End;
	End;
End;

{ orismos timis se metabliti,
	syntax: let var = expr }
Procedure CmdLet(parms : String);
Var	VarName : String;
	expr	: String;
	idx		: Integer;
Begin
if IsAlpha(parms[1]) then
	Begin
	idx := 1;	
	VarName := StrUpCase(GetNextKeyword(parms, idx));
	expr := LTrim(Copy(parms, idx, Length(parms)));
	if expr[1] = '=' then
		Begin
		expr := LTrim(Copy(expr, 2, Length(expr)));		
		VarVal[Ord(VarName[1])] := Eval(expr);
		End
	else
		RTError('POU EINAI TO =; / SWSTH SYNTAXH: LET v = expr');
	End
else
	RTError('LATHOS ONOMA METABLHTHS / SWSTH SYNTAXH: LET v = expr');
End;

{ metafora tis ektelesis
	syntax: goto linenum }
Procedure CmdGoTo(parms : String);
Var		i, lineNo, index : Integer;
Begin
lineNo := Trunc(Eval(parms));

i := 1;
index := 0;
While (i <= ProgLinesCount) AND (index = 0) Do
	Begin
	if ProgLines[i].lineNo = lineNo Then
		index := i;
	i := i + 1;
	End;

if index = 0 then
	RTError('GOTO: H GRAMMH ' + parms + ' DEN YPARXEI')
else
	CurProgLine := index - 1; { -1 giati tha auksithei kata ena }
End;

{ metafora tis ektelesis me dinatotia epistrofis (bl. return) 
	syntax: gosub linenum }
Procedure CmdGoSub(parms : String);
Var		i, lineNo, index : Integer;
Begin
lineNo := Trunc(Eval(parms));

i := 1;
index := 0;
While (i <= ProgLinesCount) AND (index = 0) Do
	Begin
	if ProgLines[i].lineNo = lineNo Then
		index := i;
	i := i + 1;
	End;

if index = 0 then
	RTError('GOSUB: H GRAMMH ' + parms + ' DEN YPARXEI')
else
	Begin
	ExecPush(CurProgLine);
	CurProgLine := index - 1; { -1 giati tha auksithei kata ena }
	End;
End;

{ epistrofi apo klisi gosub
	syntax: return }
Procedure CmdReturn(parms : String);
Var	index : Integer;
Begin
index := ExecPop;
if not RTErrorStatus then
	CurProgLine := index; { -1 giati tha auksithei kata ena }
End;

{ if 
	syntax: IF expr THEN command }
Procedure CmdIf(parms : String);
Var		expr, cmd : String;
		tp : Integer;
Begin
tp := Pos(' THEN ', StrUpCase(parms));
if tp = 0 then
	RTError('IF XWRIS THEN!')
else
	Begin
	expr := Copy(parms, 1, tp);
	if Eval(expr) <> 0 then
		Begin
		tp := tp + 6;
		expr := LTrim(Copy(parms, tp, Length(parms)));
		cmd := GetNextKeyword(expr, tp);
		expr := LTrim(Copy(expr, tp, Length(expr)));
		if NOT Execute(StrUpCase(cmd), expr) then
			CurProgLine := -1;		{ run-time error }
		End;
	End;
End;

{ emfanisi programmatos xristi }
Procedure CmdList(parms : String);
Var	i	: Integer;
Begin
For i := 1 To ProgLinesCount Do
	WriteLn(ProgLines[i].lineNo:4, ' ', ProgLines[i].Code);
End;

{ ektelesi programmatos xristi }
Procedure CmdRun(parms : String);
Var	idx  : Integer;
	cmd, inp : String;
Begin
CurProgLine := 1; { instruction pointer }
While (CurProgLine <= ProgLinesCount) AND (CurProgLine >= 1) Do
	Begin
	if TraceStatus Then Write('<', ProgLines[CurProgLine].LineNo, '>');
	idx := 1;
	inp := ProgLines[CurProgLine].Code;
	cmd := GetNextKeyword(inp, idx);
	inp := LTrim(Copy(inp, idx, Length(inp)));
	if not Execute(StrUpCase(cmd), inp) then
		CurProgLine := 0		{ run-time error }
	else
		CurProgLine := CurProgLine + 1;	
	End;
End;

{ apothikeusi }
Procedure CmdSave(parms : String);
Var	FileName : String;
	TBFile   : Text;
	i        : Integer;
Begin
FileName := LTrim(parms);
if Length(FileName) > 0 then
	Begin
	Assign(TBFile, FileName);
	Rewrite(TBFile);
	For i := 1 To ProgLinesCount Do
		WriteLn(TBFile, ProgLines[i].lineNo, ' ', ProgLines[i].Code);
	Close(TBFile);
	End;
End;

{ anagnwsi 
	note: an den exei line numbers to ektelei amesa }
Procedure CmdLoad(parms : String);
Var	FileName : String;
	TBFile   : Text;
	idx, lineNo, valCode : Integer;
	cmd, inp  : String;
	
Begin
ResetBASIC;
FileName := LTrim(parms);
if Length(FileName) > 0 then
	Begin
	Assign(TBFile, FileName);
	Reset(TBFile);
	While NOT EOF(TBFile) Do
		Begin
		ReadLn(TBFile, inp);
		
		{ store OR execute }
		idx := 1;
		cmd := GetNextKeyword(inp, idx);
		inp := LTrim(Copy(inp, idx, Length(inp)));
		
		if IsInt(cmd) then
			Begin
			Val(cmd, lineNo, valCode);
			if (lineNo > 0) AND (lineNo <= 32000) then
				SetProgLine(lineNo, inp)
			else
				RTError('AKYROS ARITHMOS GRAMMHS. KANE XRHSH 1..32000');
			End
		else 
			if not Execute(StrUpCase(cmd), inp) then
				Halt(1); { script's quit }
		End;
	Close(TBFile);
	End;
End;

{ auti den kanei tpt... alla mporei na xrisimopoiithei gia debug (p.x. na deixnei ta comments kai to line no) }
Procedure CmdNOP(msg : String);
Begin
End;

{ --- PART IV --- kentrikos kwdikas (main, userinput) ----------------------- }

{ ektelesi entolis }
Function Execute(cmd : String; params : String) : Boolean;
Begin
if cmd = 'REM' then CmdNOP(params)
else if (cmd = 'PRINT') OR (cmd = 'PR') then CmdPrint(params)
else if cmd = 'INPUT' then CmdInput(params)
else if cmd = 'LET' then CmdLet(params)
else if cmd = 'IF' then CmdIf(params)
else if cmd = 'GOTO' then CmdGoTo(params)
else if cmd = 'GOSUB' then CmdGoSub(params)
else if cmd = 'RETURN' then CmdReturn(params)
else if cmd = 'LIST' then CmdList(params)
else if cmd = 'SAVE' then CmdSave(params)
else if cmd = 'LOAD' then CmdLoad(params)
else if cmd = 'RUN' then CmdRun(params)
else if (cmd = 'NEW') OR (cmd = 'CLEAR') then CmdNew(params)
else if cmd = 'END' then CurProgLine := -1
else if cmd = 'TRON' then TraceStatus := true
else if cmd = 'TROFF' then TraceStatus := false
else if cmd = 'HELP' then DispHelp
else if cmd = 'QUIT' then Halt(1)
else RTError('AGNWSTH ENTOLH');

{ elegxos sfalmatos }	
if RTErrorStatus then
	Begin
	Execute := false;
	RTErrorStatus := false;
	End
else
	Execute := true;
End;

{ eisagwgh entolwn apo ton xristi kai ektelesi }
Procedure ConsoleInput;
Var	inp, cmd	: String;
	quit		: Boolean;
	idx			: Integer;
	lineNo, valCode : Integer;

Begin
quit := false;
Repeat
	{ Diabase tin entoli apo ton xristi }
	Write('>');
	ReadLn(inp);
	inp := LTrim(inp);

	{ ektelese }
	if Length(inp) > 0 then
		Begin
		idx := 1;
		cmd := GetNextKeyword(inp, idx);
		inp := LTrim(Copy(inp, idx, Length(inp)));
		
		if IsInt(cmd) then	{ an einai arithmos tote prepei na apothikeutei stin mnimi }
			Begin
			Val(cmd, lineNo, valCode);
			if (lineNo > 0) AND (lineNo <= 32000) then
				SetProgLine(lineNo, inp)
			else
				RTError('AKYROS ARITHMOS GRAMMHS. KANE XRHSH 1..32000');
			End
		else { einai entoli gia apeutheias ektelesi }
			Execute(StrUpCase(cmd), inp);
		End;
Until quit;
End;

{ kentriko }
Begin
Randomize;
WriteLn('TinyBASIC se Pascal (TBP), Nikolas Xristopoulos, Feb 2005, ver ', TBVersion);
WriteLn('	pliktrologise `HELP` gia ... odigies...');
CmdNew('');
ConsoleInput;
End.
