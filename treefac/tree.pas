{ Tree fractal }
Program TreeFractal(input, output);
Uses Graph, crt;

Var
	rand    : Boolean;		{ false = symmetriko, true = tyxaio sfalma }
	brafan  : Real;			{ mikos kladiou }
	inpfac  : Real;			{ apostasi kladiwn metaksy tous }
	inphf   : Real;			{ syntelestis mikrinsis kladiou apogonou }
	depth   : Real;			{ bathos: arithmos kombwn (kladi se kladi) }
	density : Real;			{ piknwtita: arithmos kladiwn ana kombo (kladia ana kladi) }

Const BranchColor = 2;    { xrwma kladiou }
Const FlowerColor = 12;   { xrwma louloudiou }

{	allagi se graphics mode
	Sim: sto directory tou programmatos tha prepei na yparxei
		kai to arxeio egavga.bgi }
Procedure InitGraphDev;
Var
	bgiDriver, bgiMode, bgiError : Integer;
Begin
bgiDriver := DETECT;
InitGraph(bgiDriver, bgiMode, '');
bgiError := GraphResult;
if bgiError <> grOK then
	Begin
	WriteLn('Graphics driver error: ', GraphErrorMsg(bgiError));
	Halt(1);	{ amesi eksodos }
	End;
End;

{ epistrofi se text mode }
Procedure CloseGraphDev;
Begin
CloseGraph;
End;

{ Perimenei na patithei ena pliktro
  An patithei ESC tote diakoptei to programma me Halt (amesi eksodos) }
Procedure Pause;
Var	ch : Char;
Begin
{ diabase ola ta pliktra apo tin keyboard buffer }
While KeyPressed Do ch := ReadKey;
{ perimene na patithei ena pliktro }
ch := ReadKey;
if ch = #27 then { an patithike to [ESC] kleise to programma }
	Halt(1);
End;

{ Epistrefei tyxaio arithmo me pedio timwn 0..1 }
Function RND : Real;
Begin
RND := Random(1000) / 1000;
End;

{ the fractal
	prosekse... edw exei nested procedures }
Procedure TreeMain;
Var
	si : Array [0..9] of Integer;
	sx, sy, st : Array [0..9] of Real;
	shf, sa, sb, sh : Array [0..9] of Real;
	sd : Array [0..9] of Real;
	sp, i : Integer;
	x, y, l, a, h, th, hf, start : Real;
	nx, ny : Integer;
	xMax, yMax : Integer;

	Procedure Branch;
	Begin
	sa[sp] := a;
	sb[sp] := brafan;
	sh[sp] := h;
	sd[sp] := depth;
	if not rand then
		Begin
		brafan := brafan * inpfac;
		h := h * hf;
		End
	else
		Begin
		a := a - th/2 + RND * th + 1;
		brafan := brafan * inpfac;
		h := h * hf * RND;
		End;
	depth := depth - 1;
	End;

	{ apothikeusi timwn stin stack }
	Procedure PushVals;
	Begin
	si[sp]  := i;
	sx[sp]  := x;
	sy[sp]  := y;
	st[sp]  := th;
	shf[sp] := hf;
	sp := sp + 1;
	End;

	{ diabasma timwn apo tin stack }
	Procedure PopVals;
	Begin
	sp := sp - 1;
	i  := si[sp];
	x  := sx[sp];
	y  := sy[sp];
	th := st[sp];
	hf := shf[sp];
	brafan := sb[sp];
	End;

	{ sxediasmos tou dentrou }
	Procedure Tree;
	Begin
	if depth = 0 then	{ zwgrafise to "louloudi" }
		Begin
		PutPixel(nx - 2, yMax-ny, FlowerColor);
		PutPixel(nx, yMax - (ny + 3), FlowerColor);
		PutPixel(nx + 2, yMax - ny, FlowerColor);
		MoveTo(nx, yMax - ny);
		End
	else				{ zwgrafise to kladi }
		Begin
		PushVals;
		start := a - brafan / 2;
		th := brafan / (density-1);
		if depth <= 2 Then
			hf := inphf / 2
		else
			hf := inphf;

		x := h * cos(a*l);
		y := h * sin(a*l);
		a := start;
		i := 1;

		While i <= density Do
			Begin
			nx := nx + Trunc(x);
			ny := ny + Trunc(y);
			SetColor(BranchColor);
			LineTo(nx, ymax-ny);
			Branch;
			Tree;
			a := sa[sp];
			brafan := sb[sp];
			h := sh[sp];
			depth := sd[sp];
			a := a + th;
			nx := nx - Trunc(x);
			ny := ny - Trunc(y);
			PutPixel(nx, yMax-ny, BranchColor);
			MoveTo(nx, yMax-ny);
			i := i + 1;
			End;

		PopVals;
		End;
	End;

{ TreeMain }
Begin
{ graphics device info }
xMax  := GetMaxX;
yMax  := GetMaxY;

{ metablites gia taxitita }
l  := PI / 180;
a  := 90;
h  := yMax div 4;
nx := xMax div 2;
ny := 1;

{ start }
MoveTo(xMax div 2, yMax);
Tree;
End;

{ Orismos twn parametrwn tou dentrou }
Procedure SetTreeParams;
Begin
rand    := true;		{ false = symmetriko, true = tyxaio sfalma }
brafan  := GetMaxY / 5;	{ mikos kladiou }
inpfac  := 0.75;		{ apostasi kladiwn metaksy tous }
inphf   := 0.8;			{ syntelestis mikrinsis kladiou apogonou }
depth   := 6;			{ bathos: arithmos kombwn (kladi se kladi) }
density := 4;			{ piknwtita: arithmos kladiwn ana kombo (kladia ana kladi) }
End;

{ kentriko programma }
Begin
Randomize;		{ epanakathorise tin genitria tyxaiwn arithmwn }
InitGraphDev;	{ gyrna se graphics mode }
SetTreeParams;	{ orismos timwn }
TreeMain;		{ zwgrafise to fractal }
Pause;			{ perimene apo ton xristi na patisei ena pliktro }
CloseGraphDev;	{ gyrna se text mode }
End.

