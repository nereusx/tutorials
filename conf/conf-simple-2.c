/* -*- tab-width: 4; -*-
 * 
 *	simple conf module - loads configuration files
 * 
 *	Copyright (C) 2024 Nicholas Christopoulos.
 *
 *	This is free software: you can redistribute it and/or modify it under
 *	the terms of the GNU General Public License as published by the
 *	Free Software Foundation, either version 3 of the License, or (at your
 *	option) any later version.
 *
 *	It is distributed in the hope that it will be useful, but WITHOUT ANY
 *	WARRANTY; without even the implied warranty of MERCHANTABILITY or
 *	FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
 *	for more details.
 *
 *	You should have received a copy of the GNU General Public License
 *	along with it. If not, see <http://www.gnu.org/licenses/>.
 *
 * 	Written by Nicholas Christopoulos <nereus@freemail.gr>
 */

#include <stdio.h>
#include <string.h>
#include <ctype.h>
#include <stdlib.h>
#include <limits.h>

#define VT_ALLOC 16
typedef struct {
	char *name;
	char *value;
	} rc_var_t;

static rc_var_t *rc_vtable = NULL;
static size_t rc_count = 0;

// store variable in memory
static void rc_add_var(const char *name, const char *value) {
	if ( (rc_count % VT_ALLOC) == 0 ) {
		size_t blocks = (rc_count / VT_ALLOC) + 1;
		rc_vtable = (rc_var_t *) realloc(rc_vtable, sizeof(rc_var_t) * VT_ALLOC * blocks + 1);
		}
	rc_vtable[rc_count].name  = strdup(name);
	rc_vtable[rc_count].value = strdup(value);
	rc_count ++;
	}

// get the value of a variable;
// if variable does not exist, returns the 'defval'
const char *rc_getval(const char *name, const char *defval) {
	register int i;
	for ( i = 0; i < rc_count; i ++ ) {
		if ( strcmp(rc_vtable[i].name, name) == 0 )
			return rc_vtable[i].value;
		}
	return defval;
	}

// set the value of a variable
// if variable does not exist, append it to array
void rc_setval(const char *name, const char *value) {
	register int i;
	for ( i = 0; i < rc_count; i ++ ) {
		if ( strcmp(rc_vtable[i].name, name) == 0 ) {
			free(rc_vtable[i].value);
			rc_vtable[i].value = strdup(value);
			return;
			}
		}
	rc_add_var(name, value);
	}

// read configuration file
// returns 0 if config file not found or cannot open.
int rc_read(const char *rc) {
	register int  line = 0;
	register char *p, *value;
	char	buf[LINE_MAX];
	int		len;
	char	*eow, *name;
	FILE	*fp;
	
	if ( (fp = fopen(rc, "r")) != NULL ) {
		while ( fgets(buf, LINE_MAX, fp) ) {
			line ++;
			p = buf;
			while ( isspace(*p) ) p ++;
			if ( *p == '\0' || *p == '#' ) continue; // empty line or comment
			if ( isalpha(*p) || *p == '_' ) {
				name = p;
				while ( isalnum(*p) || *p == '_' ) p ++;
				eow = p;	// mark end-of-keyword
				while ( isspace(*p) ) p ++;
				if ( *p != '=' ) {
					fprintf(stderr, "missing '=' at line %d\n", line);
					continue;
					}
				*eow = '\0';
				value = (p + 1);
				while ( isspace(*value) ) value ++; // optional, left-trim				
				len = strlen(value);
				if ( len && value[len-1] == '\n' ) // remove last '\n' 
					value[len-1] = '\0';
				// store it
				rc_setval(name, value);
				}
			else
				fprintf(stderr, "invalid variable name at line %d\n", line);
			}
		fclose(fp);
		return 1;
		}
	// else, no config file. you can have several configs so, do not put error here
	return 0;
	}

// clear memory of rc variables
void rc_clean() {
	register int i;
	for ( i = 0; i < rc_count; i ++ ) {
		free(rc_vtable[i].name);
		free(rc_vtable[i].value);
		}
	free(rc_vtable);
	rc_count = 0;
	rc_vtable = NULL;
	}

// --------------------------------------------------------------------------------
#ifdef TEST
// --------------------------------------------------------------------------------

// print variables
void rc_print() {
	register int i;
	for ( i = 0; i < rc_count; i ++ )
		printf("%s=\"%s\"\n", rc_vtable[i].name, rc_vtable[i].value);
	}

// === main ===
int main(int argc, char *argv[]) {
	char rcfile[PATH_MAX];

	// load file
	snprintf(rcfile, PATH_MAX, "%s/conf-example.rc", getenv("HOME"));
	if ( !rc_read(rcfile) ) {
		snprintf(rcfile, PATH_MAX, "./conf-example.rc");
		rc_read(rcfile);
		}
	rc_print();
	rc_clean();
	return 0;
	}

#endif // TEST
