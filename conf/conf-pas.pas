(* -*- tab-width: 4; -*-
 * 
 *	simple conf module - loads configuration files
 * 
 *	Copyright (C) 2024 Nicholas Christopoulos.
 *
 *	This is free software: you can redistribute it and/or modify it under
 *	the terms of the GNU General Public License as published by the
 *	Free Software Foundation, either version 3 of the License, or (at your
 *	option) any later version.
 *
 *	It is distributed in the hope that it will be useful, but WITHOUT ANY
 *	WARRANTY; without even the implied warranty of MERCHANTABILITY or
 *	FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
 *	for more details.
 *
 *	You should have received a copy of the GNU General Public License
 *	along with it. If not, see <http://www.gnu.org/licenses/>.
 *
 * 	Written by Nicholas Christopoulos <nereus@freemail.gr>
 *)

Program conf;
Uses sysutils; (* needed for trim() *)

(* Variabe type *)
Type rcVar = Record
	name  : String; (* variable name *)
	value : String; (* variable value *)
	End;

VAR
	vtable : Array of rcVar; (* Dynamic array *)
	count  : Integer = 0;	 (* Number of records in vtable *)
	alloc  : Integer = 0;	 (* Allocated size in vtable (in records) *)

Const alloc_incr = 32; (* Increment size of dynamic array allocation *)

(*
 *	Adds a new variable in vtable.
 *	Use rcSetVar instead.
 *)
Procedure rcAddVar(name, value : String);
Begin
   	if ( alloc <= count + 1 ) then begin
		alloc := alloc + alloc_incr;
		SetLength(vtable, alloc);
		end;
	vtable[count].name  := name; (* first position is 0 *)
	vtable[count].value := value;
    count := count + 1;
End;

(*
 *	Get the value of a variable;
 *	if variable does not exist, returns the 'defval'
 *)
Function rcGetVar(name, defval : String) : String;
var i : Integer;
Begin
	if ( count > 0 ) then 
		for i := 0 to count - 1 do
			if ( vtable[i].name = name ) then
				Exit(vtable[i].value);
	rcGetVar := defval;
End;

(*
 *	Set the value of a variable.
 *	If variable does not exist, append it to array.
 *)
Procedure rcSetVar(name, value : String);
var i : Integer;
Begin
	if ( count > 0 ) then begin
		for i := 0 to count - 1 do begin
			if ( vtable[i].name = name ) then begin
				vtable[i].value := value;
				Exit;
				end;
			end;
		end;
	rcAddVar(name, value);
End;

(*
 *	Read configuration file.
 *	Returns false if config file not found or cannot open.
 *)
Function rcRead(rcfile : String) : Boolean;
var	fp : Text;
	buf : String;
	i, line : Integer;
Begin
	Assign(fp, rcfile);
	{$I-}
	Reset(fp);
	{$I+}
	if ( IOResult <> 0 ) then
		Exit(false);
	line := 0;
	while not EOF(fp) do begin
		ReadLn(fp, buf);
		if ( buf[1] = '#' ) then Continue;
		line := line + 1;
		if ( Length(buf) > 0 ) then begin
			i := Pos('=', buf);
			if ( i > 0 ) then
				rcSetVar( Trim(Copy(buf, 1, i - 1)), Trim(Copy(buf, i + 1, Length(buf) - i)) )
			else
				WriteLn('missing separator "=" at line ', line);
		end;
	end;
	rcRead := true;
End;

(* -------------------------------------------------------------------------------- *)
{$ifdef TEST}
(* -------------------------------------------------------------------------------- *)

(* print the list of variables *)
Procedure rcPrint;
var i : Integer;
Begin
	if ( count > 0 ) then
		for i := 0 to count - 1 do
			WriteLn(vtable[i].name, '=', vtable[i].value);
End;

(* main *)
Begin
	if ( rcRead('./conf-example.rc') ) then
		rcPrint
	else
		WriteLn('no rc file found');
End.

{$endif}

