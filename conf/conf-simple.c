/* -*- tab-width: 4; -*-
 * 
 *	simple conf module - loads configuration files
 * 
 *	Copyright (C) 2024 Nicholas Christopoulos.
 *
 *	This is free software: you can redistribute it and/or modify it under
 *	the terms of the GNU General Public License as published by the
 *	Free Software Foundation, either version 3 of the License, or (at your
 *	option) any later version.
 *
 *	It is distributed in the hope that it will be useful, but WITHOUT ANY
 *	WARRANTY; without even the implied warranty of MERCHANTABILITY or
 *	FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
 *	for more details.
 *
 *	You should have received a copy of the GNU General Public License
 *	along with it. If not, see <http://www.gnu.org/licenses/>.
 *
 * 	Written by Nicholas Christopoulos <nereus@freemail.gr>
 */

#include <stdio.h>
#include <string.h>
#include <ctype.h>
#include <stdlib.h>
#include <limits.h>

// read configuration file
// returns 0 if config file not found or cannot open.
int rc_read(const char *rc) {
	register int  line = 0;
	register char *p, *value;
	char	buf[LINE_MAX];
	int		len;
	char	*eow, *name;
	FILE	*fp;
	
	if ( (fp = fopen(rc, "r")) != NULL ) {
		while ( fgets(buf, LINE_MAX, fp) ) {
			line ++;
			p = buf;
			while ( isspace(*p) ) p ++;
			if ( *p == '\0' || *p == '#' ) continue; // empty line or comment
			if ( isalpha(*p) || *p == '_' ) {
				name = p;
				while ( isalnum(*p) || *p == '_' ) p ++;
				eow = p;	// mark end-of-keyword
				while ( isspace(*p) ) p ++;
				if ( *p != '=' ) {
					fprintf(stderr, "missing '=' at line %d\n", line);
					continue;
					}
				*eow = '\0';
				value = (p + 1);
				while ( isspace(*value) ) value ++; // optional, left-trim				
				len = strlen(value);
				if ( len && value[len-1] == '\n' ) // remove last '\n' 
					value[len-1] = '\0';
				// print the resule
				printf("%s=\"%s\"\n", name, value);
				}
			else
				fprintf(stderr, "invalid variable name at line %d\n", line);
			}
		fclose(fp);
		return 1;
		}
	// else, no config file. you can have several configs so, do not put error here
	return 0;
	}

// --------------------------------------------------------------------------------
#ifdef TEST
// --------------------------------------------------------------------------------

// === main ===
int main(int argc, char *argv[]) {
	char rcfile[PATH_MAX];

	// load file
	snprintf(rcfile, PATH_MAX, "%s/conf-example.rc", getenv("HOME"));
	if ( !rc_read(rcfile) ) {
		snprintf(rcfile, PATH_MAX, "./conf-example.rc");
		rc_read(rcfile);
		}
	return 0;
	}

#endif // TEST
