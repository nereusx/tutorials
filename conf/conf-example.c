/* -*- tab-width: 4; -*-
 * 
 *	conf module - loads configuration files
 * 
 *	Copyright (C) 2024 Nicholas Christopoulos.
 *
 *	This is free software: you can redistribute it and/or modify it under
 *	the terms of the GNU General Public License as published by the
 *	Free Software Foundation, either version 3 of the License, or (at your
 *	option) any later version.
 *
 *	It is distributed in the hope that it will be useful, but WITHOUT ANY
 *	WARRANTY; without even the implied warranty of MERCHANTABILITY or
 *	FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
 *	for more details.
 *
 *	You should have received a copy of the GNU General Public License
 *	along with it. If not, see <http://www.gnu.org/licenses/>.
 *
 * 	Written by Nicholas Christopoulos <nereus@freemail.gr>
 */

#include <stdio.h>
#include <string.h>
#include <ctype.h>
#include <stdlib.h>
#include <limits.h>

// definition of rc-variable
typedef struct {
	const char *name;	// variable name
	char type;			// pointer type
	void *value;		// pointer to data
	} rc_var_t;

// global, table of variables
rc_var_t *rc_var_table;

// returns true if digit 'c' is an octal ascii digit
static inline int isoctaldigit(int c) {return (c >= '0' && c <= '7');}

// returns the output of system command 'cmd' as newly allocated string or NULL on error
static char *rc_syspipe(const char *cmd) {
	FILE *fp;
	char	*dest = NULL, buf[LINE_MAX];
	int		alloc = 0, l;
	
	if ( (fp = popen(cmd, "r")) == NULL )
		return NULL; // error, propably in the command 'cmd'
	while ( fgets(buf, LINE_MAX, fp) ) {
		l = strlen(buf);
		if ( l ) {
			if ( dest ) {
				alloc += l;
				dest = (char *) realloc(dest, alloc + 1);
				strcat(dest, buf);
				}
			else { // first time
				alloc = l;
				dest = (char *) malloc(alloc + 1);
				strcpy(dest, buf);
				}
			}
		}
	if ( dest ) { // remove last '\n'
		l = strlen(dest);
		if ( dest[l-1] == '\n' )
			dest[l-1] = '\0';
		}
	pclose(fp);
	return dest;
	}

// returns a newly allocated string with the value of the variable 'name'
// if variable does not exists returns an empty newly allocated string.
static char *rc_getenv(const char *name) {
	char	*ptr;

	// local variables
	for ( int i = 0; rc_var_table[i].name; i ++ ) {
		if ( strcmp(rc_var_table[i].name, name) == 0 ) {
			// dynamic string
			if ( (rc_var_table[i].type == '*') || (rc_var_table[i].type == '$') ) {
				ptr = *((char **) rc_var_table[i].value);
				if ( ptr )
					return strdup(ptr);
				return strdup("");
				}
			
			// not a string
			ptr = (char *) malloc(LINE_MAX);
			ptr[0] = '\0';
			switch ( rc_var_table[i].type ) {
			case 's': snprintf(ptr, LINE_MAX, "%s", (char *) rc_var_table[i].value); break;
			case 'i': case 'b': snprintf(ptr, LINE_MAX, "%d", *((int *) rc_var_table[i].value)); break;
			case 'f': snprintf(ptr, LINE_MAX, "%lf", *((double *) rc_var_table[i].value)); break;
				}
			return ptr;
			}
		}

	// not found - environment variables
	ptr = getenv(name);
	if ( ptr )
		return strdup(ptr);
	return strdup("");
	}

//	expand string
//	
//	1. single quotes
//	2. escape characters
//	3. local variables (form $xxx and ${xxxx})
//	4. environment variables (form $xxx and ${xxxx})
//	5. shell output $(cmdline)
//	todo: 6. reverse quotes (shell output)
//	7. double quotes
//
//	PARAMETERS
//		source	= the source code
//		&size	= if not NULL it is the length of 'source' which processed
//		plevel	= if set, exit on `plevel' number of ')'
//		blevel	= if set, exit on 'blevel' number of '}'
//
//	RETURNS a newly allocated string
static char *rc_expand(const char *source, size_t *psize, int plevel, int blevel)	{
	register const char *p = source;
	register char	*r;
	char	*result = NULL;
	int		sq = 0, dq = 0;
	int		n, i;

	if ( source == NULL )
		return strdup("");

	int		srclen = strlen(source);
	int		alloc = srclen;
	if ( alloc < 2 ) {
		if ( psize ) *psize = srclen;
		return strdup(source);
		}

	r = result = (char *) malloc(alloc+1);
	while ( *p ) {

		// quotation
		if ( sq ) { // in single-quoted string
			if ( *p == '\'' ) {
				sq = 0;
				p ++;
				continue;
				}
			*r ++ = *p ++;
			continue;
			}
		
		switch ( *p ) {
		case '\'':
			sq = 1;
			p ++;
			continue;

		case '#': // comments
			*r = '\0';
			if ( psize ) *psize = (p - source) + 1;
			return result;

		// escape characters
		case '\\':
			p ++;
			switch ( *p ) {
			case '\0':	*r = '\0';
				fprintf(stderr, "continue line; error: it is not supported\n");
				if ( psize ) *psize = (p - source) + 1;
				*r = '\0';
				return result;	// continue line; error: it is not supported
			case 'a':	*r ++ = '\a'; break;	// beep, char 7
			case 'b':	*r ++ = '\b'; break;	// backspace, always 8
				// terminal may use del char for backspace, thanks Denis, it is not C's problem
			case 't':	*r ++ = '\t'; break;
			case 'r':	*r ++ = '\r'; break;
			case 'n':	*r ++ = '\n'; break;
			case 'v':	*r ++ = '\v'; break;
			case 'f':	*r ++ = '\f'; break;
			case 'e':	*r ++ = '\x1b'; break;
			case 's':	*r ++ = ' '; break;		// space, new in C99 ?
			case 'd':	*r ++ = '\x7f'; break;	// del character, new in C99 ?
			case '0': case '1': case '2': case '3':
			case '4': case '5': case '6': case '7': // octal
				n = 0;
				for ( i = 0; i < 3; i ++ ) {
					if ( isoctaldigit(*p) ) {
						n = (n << 3) | (*p - '0');
						p ++;
						}
					else
						break;
					}
				*r ++ = n;
				continue; // do not increase 'p'
			case 'x': // hexadecimal
				p ++;
				if ( isxdigit(*p) ) {
					n = 0;
					for ( i = 0; i < 2; i ++ ) {
						if ( isxdigit(*p) ) {
							if ( *p >= 'a' )
								n = (n << 4) | ((*p - 'a') + 10);
							else if ( *p >= 'A' )
								n = (n << 4) | ((*p - 'A') + 10);
							else
								n = (n << 4) | (*p - '0');
							p ++;
							}
						else
							break;
						}
					*r ++ = n;
					}
				continue; // do not increase 'p'
			default:
				*r ++ = *p;
				}
			p ++;
			continue;

		// wait close of parenthesis
		case '(':
			if ( plevel )	plevel ++;
			break;
		case ')':
			if ( plevel ) {
				if ( -- plevel == 0 ) {
					*r = '\0';
					if ( psize ) *psize = (p - source) + 1;
					return result;
					}
				}
			break;
		
		// wait close of braces
		case '{':
			if ( blevel )	blevel ++;
			break;
		case '}':
			if ( blevel ) {
				if ( -- blevel == 0 ) {
					*r = '\0';
					if ( psize ) *psize = (p - source) + 1;
					return result;
					}
				}
			break;

		// handle $ expressions (environment variables)
		case '$': {
			char *d, *dest, *val = NULL;
			size_t	size = 0;
			
			p ++;
			if ( isalnum(*p) || *p == '_' ) {
				d = dest = (char *) malloc(srclen);
				while ( isalnum(*p) || *p == '_' )
					*d ++ = *p ++;
				*d = '\0';
				size = strlen(dest);
				val = rc_getenv(dest);
				}
			else if ( *p == '{' ) {
				p ++;
				dest = rc_expand(p, &size, 0, 1);
				p += size;
				val = rc_getenv(dest);
				}
			else if ( *p == '(' ) {
				p ++;
				dest = rc_expand(p, &size, 1, 0);
				p += size;
				val = rc_syspipe(dest);
				}
			else { // error
				*r ++ = '$';
				*r ++ = *p ++;
				continue;
				}
			
			// copy the results
			if ( val ) {
				size_t	pos = r - result;
				alloc += (strlen(val) - size) + 1;
				result = (char *) realloc(result, alloc);
				r = result + pos;
				d = val;
				while ( *d ) *r ++ = *d ++;
				free(val);
				}
			free(dest);
			continue;
			} // internal block
			}; // switch

		// handle double-quoted string
		if ( *p == '"' ) {
			if ( dq ) {
				dq = 0;
				p ++;
				continue;
				}
			else {
				dq = 1;
				p ++;
				continue;
				}
			}

		// normal character
		*r ++ = *p ++;
		}

	*r = '\0';
	if ( psize ) *psize = p - source;
	return result;
	}

// assign variables
static void rc_varset(int line, const char *variable, const char *value) {
	int n, i;
	for ( i = 0; rc_var_table[i].name; i ++ ) {
		if ( strcmp(rc_var_table[i].name, variable) == 0 ) {
			switch ( rc_var_table[i].type ) {
			case '*': case '$':
				if ( value )
					*((char **)(rc_var_table[i].value)) = ( rc_var_table[i].type == '$' ) ?
						rc_expand(value, NULL, 0, 0) : strdup(value);
				else { // free previous value
					if ( *((char **)(rc_var_table[i].value)) )
						free(*((char **)(rc_var_table[i].value)));
					*((char **)(rc_var_table[i].value)) = NULL;
					}
				break;
			case 's':
				if ( value )
					strcpy((char *)(rc_var_table[i].value), value);
				else
					strcpy((char *)(rc_var_table[i].value), "");					
				break;
			case 'i': case 'b':
				if ( isdigit(*value) )
					n = atoi(value);
				else {
					if      ( strcasecmp(value, "off") == 0 ) n = 0;
					else if ( strcasecmp(value, "on")  == 0 ) n = 1;
					else if ( strcasecmp(value, "false")  == 0 ) n = 0;
					else if ( strcasecmp(value, "true")  == 0 ) n = 1;
					else n = atoi(value);
					}
				*((int *)(rc_var_table[i].value)) = n;
				break;
			case 'f':
				*((double *)(rc_var_table[i].value)) = atof(value);
				break;
				}
			return;
			}
		}
	fprintf(stderr, "rc(%d): uknown variable [%s]\n", line, variable);
	}

// parse string line
static void rc_parseline(int line, const char *source) {
	char name[LINE_MAX], *d = name;
	const char *p = source;
	while ( isblank(*p) ) p ++;	// skip spaces
	if ( *p && *p != '#' ) {	// ignore empty or comment lines
		// copy first word to 'name'
		while ( *p == '_' || isalnum(*p) )
			*d ++ = *p ++;
		*d = '\0';
		
		while ( isblank(*p) ) p ++;	// skip spaces
		if ( *p == '=' ) {	// it must be assign symbol '=', other cases even commands can be added
			p ++;
			while ( isblank(*p) ) p ++;	// skip spaces
			rc_varset(line, name, p);
			}
		else
			fprintf(stderr, "syntax error in line %d\n", line);
		}
	}

// main function:
// read configuration file
// returns 0 if config file not found or cannot open.
int rc_read(const char *rc) {
	int		line = 0, len;
	char	buf[LINE_MAX];
	const char *p;
	FILE	*fp;
	
	if ( (fp = fopen(rc, "r")) != NULL ) {
		while ( fgets(buf, LINE_MAX, fp) ) {
			line ++;
			len = strlen(buf);
			if ( len ) {
				p = buf;
				while ( isspace(*p) ) p ++;
				if ( *p == '\0' ) continue; // empty line
				if ( *p == '#' ) continue;  // comments line
				if ( buf[len-1] == '\n' )	// remove last '\n' 
					buf[len-1] = '\0';
				rc_parseline(line, p);
				}
			}
		fclose(fp);
		return 1;
		}
	// else, no config file. you can have several configs so, do not put error here
	return 0;
	}

// --------------------------------------------------------------------------------
#ifdef TEST
// --------------------------------------------------------------------------------

// options - example
char	*opt_pstr = NULL;
char	*opt_estr = NULL;
char	opt_str[LINE_MAX];
int		opt_bool = 0;
int		opt_int = 0;
double	opt_float = 0.0;

// table of variables
rc_var_t rc_local_var_table[] = {
	{ "strvarptr", '*', &opt_pstr },
	{ "stringvar", 's', opt_str },
	{ "boolvar",   'b', &opt_bool },
	{ "intvar",    'i', &opt_int },
	{ "floatvar",  'f', &opt_float },
	{ "expandedstr", '$', &opt_estr },
	{ NULL, '\0', NULL } }; // mark - end-of-table

// print variables (debug)
void rc_print() {
	char	*ptr;
	for ( int i = 0; rc_var_table[i].name; i ++ ) {
		printf("%s = ", rc_var_table[i].name);
		switch ( rc_var_table[i].type ) {
		case '*': case '$':
			ptr = *((char **) rc_var_table[i].value);
			if ( ptr )
				printf("\"%s\"\n", ptr);
			else
				printf("NULL\n");
			break;
		case 's': printf("\"%s\"\n", (char *) rc_var_table[i].value); break;
		case 'i': case 'b': printf("%d\n", *((int *) rc_var_table[i].value)); break;
		case 'f': printf("%lf\n", *((double *) rc_var_table[i].value)); break;
			}
		}
	}

// === main ===
int main(int argc, char *argv[]) {
	char rcfile[PATH_MAX];

	// init
	rc_var_table = rc_local_var_table;

	// load file
	snprintf(rcfile, PATH_MAX, "%s/conf-example.rc", getenv("HOME"));
	if ( !rc_read(rcfile) ) {
		snprintf(rcfile, PATH_MAX, "./conf-example.rc");
		rc_read(rcfile);
		}
	
	// print data
	rc_print();
	if ( opt_pstr )	free(opt_pstr);
	if ( opt_estr )	free(opt_estr);
	return 0;
	}

#endif // TEST

